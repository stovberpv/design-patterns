# Structural

## Название

TODO :

## Назначение

TODO :

## Patterns

1. Adapter
1. Bridge
1. Composite
1. [Decorator](./Decorator/README.md)
1. Extension object
1. Facade
1. Flyweight
1. Front controller
1. Marker
1. Module
1. Proxy
1. Twin
