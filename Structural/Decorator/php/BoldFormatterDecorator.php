<?php

require_once 'FormatterDecorator.php';
require_once 'IText.php';

class BoldFormatterDecorator extends FormatterDecorator implements IText
{
  public function write(string $message) : void
  {
    echo '<b>';
    $this->decoratedText->write($message);
    echo '</b>';
  }
}