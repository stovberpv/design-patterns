<?php

require_once 'FormatterDecorator.php';
require_once 'IText.php';

class ItalicFormatterDecorator extends FormatterDecorator implements IText
{
  public function write(string $message) : void
  {
    echo '<i>';
    $this->decoratedText->write($message);
    echo '</i>';
  }
}