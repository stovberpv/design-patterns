<?php

interface IText
{
  public function write(string $content) : void;
}