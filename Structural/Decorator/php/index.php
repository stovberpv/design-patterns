<?php

require_once 'HeadingText.php';
require_once 'BoldFormatterDecorator.php';
require_once 'ItalicFormatterDecorator.php';

$heading = new HeadingText();
$heading->write('Some default heading');

echo "\n";

$boldHeading = new BoldFormatterDecorator($heading);
$boldHeading->write('Some bold heading');

echo "\n";

$italicHeading = new ItalicFormatterDecorator($heading);
$italicHeading->write('Some italic heading');

echo "\n";

$multiFormattedText = new BoldFormatterDecorator(new ItalicFormatterDecorator($heading));
$multiFormattedText->write('Some heading text formatted in bold and italic');