<?php

require_once 'IText.php';

class HeadingText implements IText
{
  public function write(string $content) : void
  {
    echo $content;
  }
}