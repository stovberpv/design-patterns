<?php

require_once 'IText.php';

abstract class FormatterDecorator implements IText
{
  /**
   * @var IText
   */
  protected $decoratedText;

  public function __construct(IText $text)
  {
    $this->decoratedText = $text;
  }

  public function write(string $content) : void
  {
    $this->decoratedText->write($content);
  }
}