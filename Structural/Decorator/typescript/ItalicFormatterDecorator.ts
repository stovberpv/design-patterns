import FormatterDecorator from './FormatterDecorator';
import IText from './IText';

class ItalicFormatterDecorator extends FormatterDecorator implements IText {
  public constructor(text : IText) {
    super(text);
  }

  public write(content : string) : void {
    console.log('<i>');
    this.decoratedText.write(content);
    console.log('</i>');
  }
}

export default ItalicFormatterDecorator;