import IText from './IText';

class HeadingText implements IText {
  write(content : string) : void {
    console.log(`<h1>${content}</h1>`);
  }
}

export default HeadingText;