import IText from './IText';

abstract class FormatterDecorator implements IText {
  protected decoratedText : IText;

  public constructor(text : IText) {
    this.decoratedText = text;
  }

  write(content : string) : void {
    this.decoratedText.write(content);
  }
}

export default FormatterDecorator;