import HeadingText from './HeadingText';
import BoldFormatterDecorator from './BoldFormatterDecorator';
import ItalicFormatterDecorator from './ItalicFormatterDecorator';

const heading = new HeadingText();
heading.write('Some default heading');

console.log('\n');

const boldHeading = new BoldFormatterDecorator(heading);
boldHeading.write('Some bold heading');

console.log('\n');

const italicHeading = new ItalicFormatterDecorator(heading);
italicHeading.write('Some italic heading');

console.log('\n');

const multiFormattedText = new BoldFormatterDecorator(new ItalicFormatterDecorator(heading));
multiFormattedText.write('Some heading text formatted in bold and italic');