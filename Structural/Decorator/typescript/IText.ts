interface IText {
  write(content : string) : void;
}

export default IText;