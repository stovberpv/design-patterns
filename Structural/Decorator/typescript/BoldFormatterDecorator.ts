import FormatterDecorator from './FormatterDecorator';
import IText from './IText';

class BoldFormatterDecorator extends FormatterDecorator implements IText {
  public constructor(text : IText) {
    super(text);
  }

  public write(content : string) : void {
    console.log('<b>');
    this.decoratedText.write(content);
    console.log('</b>');
  }
}

export default BoldFormatterDecorator;