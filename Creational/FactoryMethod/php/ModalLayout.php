<?php

require_once 'ILayout.php';

class ModalLayout implements ILayout
{
  public function compose() : string
  {
    return 'Modal Dialogue';
  }
}