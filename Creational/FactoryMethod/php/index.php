<?php

require_once 'ModalDialogue.php';
require_once 'InlineDialogue.php';

$modalDialogue = new ModalDialogue();
$modalDialogue->render();

echo "\n";

$inlineDialogue = new InlineDialogue();
$inlineDialogue->render();