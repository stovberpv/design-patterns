<?php

require_once 'Dialogue.php';
require_once 'ILayout.php';
require_once 'InlineLayout.php';

class InlineDialogue extends Dialogue
{
  protected function makeLayout() : ILayout
  {
    return new InlineLayout();
  }
}