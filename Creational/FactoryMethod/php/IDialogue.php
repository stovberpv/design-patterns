<?php

interface IDialogue
{
  function render() : void;
}