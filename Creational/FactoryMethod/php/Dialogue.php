<?php

require_once 'IDialogue.php';
require_once 'ILayout.php';

abstract class Dialogue implements IDialogue
{
  protected abstract function makeLayout() : ILayout;

  public function render() : void
  {
    $layout = $this->makeLayout();
    echo $layout->compose();
  }
}