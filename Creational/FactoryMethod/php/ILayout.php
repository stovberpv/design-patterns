<?php

interface ILayout
{
  function compose() : string;
}