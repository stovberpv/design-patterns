<?php

require_once 'Dialogue.php';
require_once 'ILayout.php';
require_once 'ModalLayout.php';

class ModalDialogue extends Dialogue
{
  protected function makeLayout() : ILayout
  {
    return new ModalLayout();
  }
}