<?php

require_once 'ILayout.php';

class InlineLayout implements ILayout
{
  public function compose() : string
  {
    return 'Inline Layout';
  }
}