import Dialogue from './Dialogue';
import ILayout from './ILayout';
import InlineLayout from './InlineLayout';

class InlineDialogue extends Dialogue {
  protected makeLayout() : ILayout {
    return new InlineLayout();
  }
}

export default InlineDialogue;