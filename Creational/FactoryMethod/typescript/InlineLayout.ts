import ILayout from './ILayout';

class InlineLayout implements ILayout {
  public compose() : string {
    return 'Inline layout';
  }
}

export default InlineLayout;