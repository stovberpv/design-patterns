import ILayout from './ILayout';
import IDialogue from './IDialogue';

abstract class Dialogue implements IDialogue {
  protected abstract makeLayout() : ILayout;

  public render() : void {
    const layout : ILayout = this.makeLayout();
    console.log(layout.compose());
  }
}

export default Dialogue;