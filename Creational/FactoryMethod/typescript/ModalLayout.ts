import ILayout from './ILayout';

class ModalLayout implements ILayout {
  public compose() : string {
    return 'Modal layout';
  }
}

export default ModalLayout;