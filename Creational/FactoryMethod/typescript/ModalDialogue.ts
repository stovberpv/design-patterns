import Dialogue from './Dialogue';
import ILayout from './ILayout';
import ModalLayout from './ModalLayout';

class ModalDialogue extends Dialogue {
  protected makeLayout() : ILayout {
    return new ModalLayout();
  }
}

export default ModalDialogue;