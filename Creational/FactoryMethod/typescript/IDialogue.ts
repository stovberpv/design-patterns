interface IDialogue {
  render() : void;
}

export default IDialogue;