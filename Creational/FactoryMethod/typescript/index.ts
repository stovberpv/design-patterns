import ModalDialogue from './ModalDialogue';
import IDialogue from './IDialogue';
import InlineDialogue from './InlineDialogue';

const modalDialogue : IDialogue = new ModalDialogue();
modalDialogue.render();

const inlineDialogue : IDialogue = new InlineDialogue();
inlineDialogue.render();