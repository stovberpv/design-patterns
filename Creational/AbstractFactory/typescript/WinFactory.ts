import IGUIFactory from './IGUIFactory';
import IButton from './IButton';
import WinButton from './WinButton';
import IMenu from './IMenu';
import WinMenu from './WinMenu';

class WinFactory implements IGUIFactory {
  makeButton() : IButton {
    return new WinButton();
  }
  makeMenu() : IMenu {
    return new WinMenu();
  }
}

export default WinFactory;