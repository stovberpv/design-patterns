import IGUIFactory from './IGUIFactory';
import IButton from './IButton';
import LinuxButton from './LinuxButton';
import IMenu from './IMenu';
import LinuxMenu from './LinuxMenu';

class LinuxFactory implements IGUIFactory {
  makeButton() : IButton {
    return new LinuxButton();
  }
  makeMenu() : IMenu {
    return new LinuxMenu();
  }
}

export default LinuxFactory;