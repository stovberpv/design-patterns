import IButton from './IButton';

class LinuxButton implements IButton {
  public render() : void {
    console.log('Linux Button rendered');
  }
}

export default LinuxButton;