import IButton from './IButton';
import IMenu from './IMenu';

interface IGUIFactory {
  makeButton() : IButton;
  makeMenu() : IMenu;
}

export default IGUIFactory;