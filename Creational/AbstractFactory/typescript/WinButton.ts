import IButton from './IButton';

class WinButton implements IButton {
  public render() : void {
    console.log('Win Button rendered');
  }
}

export default WinButton;