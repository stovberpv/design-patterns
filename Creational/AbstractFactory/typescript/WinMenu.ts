import IMenu from './IMenu';

class WinMenu implements IMenu {
  public render() : void {
    console.log('Win Menu rendered');
  }
}

export default WinMenu;