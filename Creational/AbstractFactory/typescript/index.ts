import IGUIFactory from './IGUIFactory';
import LinuxFactory from './LinuxFactory';
import Application from './Application';
import WinFactory from './WinFactory';

const OS = {
  WINDOWS: 'WINDOWS',
  LINUX: 'LINUX'
};

const currentOS = OS.LINUX;

let factory : IGUIFactory = null;

if (currentOS === OS.LINUX) {
  factory = new LinuxFactory();
} else if (currentOS === OS.WINDOWS) {
  factory = new WinFactory();
} else {
  throw new Error('Unknown operating system.');
}

const application : Application = new Application(factory);
application.makeUI();