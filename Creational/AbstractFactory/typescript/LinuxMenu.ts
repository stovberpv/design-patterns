import IMenu from './IMenu';

class LinuxMenu implements IMenu {
  public render() : void {
    console.log('Linux Menu rendered');
  }
}

export default LinuxMenu;