import IGUIFactory from './IGUIFactory';
import IMenu from './IMenu';
import IButton from './IButton';

class Application {
  private guiFactory : IGUIFactory;
  private menu : IMenu;
  private button : IButton

  constructor(factory : IGUIFactory) {
    this.guiFactory = factory;
  }

  public makeUI() : void {
    this.menu = this.guiFactory.makeMenu();
    this.button = this.guiFactory.makeButton();
    this.render();
  }

  private render() : void {
    this.button.render();
    this.menu.render();
  }
}

export default Application;