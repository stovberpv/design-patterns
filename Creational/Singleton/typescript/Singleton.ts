class Singleton {
  protected static instance : Singleton;

  private constructor() {}

  public static getInstance() : Singleton {
    if (!Singleton.instance) {
      Singleton.instance = new Singleton();
    }

    return Singleton.instance;
  }
}

export default Singleton;