import Singleton from './Singleton';

/* not works
const singleton = new Singleton();
const instance = singleton.getInstance();
*/

// works
const singleton = Singleton.getInstance();