# Creational

## Название

Порождающие паттерны

## Назначение

Абстрагируют процесс инстанцирования.
Могут сделать систему независимой от способа создания, композиции и представления объектов.
Паттерн, порождающий классы, использует наследование, что бы варьировать инстанцируемый класс,
а паттерн, порождающий объекты, делегирует интанцирование другому объекту.

## Patterns

1. [AbstractFactory](./AbstractFactory/README.md)
1. Builder
1. Dependency Injection
1. [FactoryMethod](./FactoryMethod/README.md)
1. Lazy initialization
1. Multiton
1. Object pool
1. Prototype
1. Resource acquisition is initialization (RAII)
1. [Singleton](./Singleton/README.md)
