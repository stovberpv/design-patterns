# Beahvioral

## Название

TODO :

## Назначение

TODO :

## Patterns

1. [Blackboard](./BlackBoard/README.md)
1. Chain of responsibility
1. Command
1. Interpreter
1. Iterator
1. Mediator
1. Memento
1. Null object
1. [Observer](./Observer/README.md)
1. Servant
1. Specification
1. State
1. [Strategy](./Strategy/README.md)
1. Template method
1. Visitor
