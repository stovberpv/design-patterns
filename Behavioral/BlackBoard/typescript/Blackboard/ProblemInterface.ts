/**
 * The problems that do not have a feasible deterministic solution for the transformation of raw data into high-level data structures.
 * When a problem decomposed into subproblems it spans several fields of  expertise.
 * The solutions to the partial problems require different representations and paradigms.
 * A solution can only be built by integrating the results of several knowledge sources.
 */
interface ProblemInterface {
  /**
   * The raw data
   * Input have different representations.
   */
  problem : any;
}

export default ProblemInterface;