import ProblemInterface from "./ProblemInterface";
import { BlackboardContributor } from "../types";

/**
 * The blackboard is  the central data store.
 * Elements of the solution space and control data are stored here.
 * The blackboard provides an interface that enables all knowledge sources to read from and write to it.
 *
 * In such cases it is often enough to find an optimal solution for most cases, and a suboptimal solution, or no solution, for the rest.
 * The limitations of a Blackboard system therefore have to be documented carefully, and if important decisions  depend  on its  results,  the results have  to  be verified.
 */
interface BlackboardInterface {
  /**
   * TODO : how to iunspect the blackboard? this is was not described.
   *
   * @returns {Array<ProblemInterface>}
   */
  inspect() : Array<ProblemInterface>;

  /**
   * TODO : how to update the blackboard? this is was not described.
   *
   * @param {BlackboardContributor} blackboardContributor
   */
  update(blackboardContributor : BlackboardContributor) : void;
}

export default BlackboardInterface;