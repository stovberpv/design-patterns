import BlackboardInterface from "./BlackBoardInterface";
import SolutionInterface from "./SolutionInterface";
import ProblemInterface from "./ProblemInterface";
import { BlackboardContributor } from "../types";

class Blackboard implements BlackboardInterface {
  /**
   * All elements of the solution space can appear on the blackboard.
   * For solutions that are constructed during the problem  solving process and put on the blackboard, we use the terms hypothesis or blackboard entry.
   * Hypotheses rejected later in  the process are removed from the blackboard.
   *
   * @description
   *
   * @type {Array<SolutionInterface>}
   * @memberof Blackboard
   */
  solutions: Array<SolutionInterface>;
  controlData: Array<ProblemInterface>;

  constructor() {
    this.solutions = [];
    this.controlData = [];
  }

  inspect() : Array<ProblemInterface> {
    return this.controlData; // FIX : how to do this properly?
  }

  update(blackboardContributor : BlackboardContributor) : void {
    blackboardContributor(this.controlData); // FIX : how to do this properly?
  }
}

export default Blackboard;