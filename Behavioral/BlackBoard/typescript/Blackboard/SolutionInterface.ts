import ProblemInterface from "./ProblemInterface";
import { AbstractionLevel } from "../types";

/**
 * The elements of the solution space.
 * Represents a partial problem and a certain stage of its solution.
 * Each transformation step can also generate several alternative  solutions.
 *
 * The set of all possible solutions is called the solution space, and is organized into levels of abstraction:
 *  - The lowest level of solution consists of an internal representation of  the input.
 *  - Potential solutions of the overall system task are on the highest level.
 *
 * For solutions that are constructed during the problem solving process and put on the blackboard, we use the terms 'hypothesis' or 'blackboard entry'.
 * Hypotheses rejected later in the process are removed from the blackboard.
 * It is often useful to specify relationships between hypotheses, such as 'part-of  or 'in-support-of.
 */
interface SolutionInterface extends ProblemInterface {
  /**
   * The conceptual distance from the input.
   * Hypotheses that have  a low abstraction level have a representation that is still similar to input data representation,
   * while hypotheses with the highest abstraction level are on the same abstraction level as the output
   */
  abstractionLevel: AbstractionLevel;
  /**
   * The intermediate and final results have different representations, and the algorithms are implemented according to different paradigms.
   */
  solution : any;
  /**
   * Hypotheses rejected later in the process are removed from the blackboard.
   */
  isRejected : boolean;
}

export default SolutionInterface;