import ProblemInterface from "../Blackboard/ProblemInterface";

/**
 * This is the conceptual distance from the input.
 */
type AbstractionLevel = {
  /**
   * Potential solutions of the overall system task are on the highest level.
   * The same abstraction level as the output.
   */
  higher: 'higher-level',
  /**
   * The lowest level of solution consists of an internal representation of  the input.
   * Similar to input data representation.
   */
  lower: 'lower-level',
  // Other hypothesis attributes are the estimated degree of truth of  the hypothesis or the time interval covered by the hypothesis.
};

type BlackboardContributor = (controlData : Array<ProblemInterface>) => Array<ProblemInterface>;

export {
  AbstractionLevel,
  BlackboardContributor
};