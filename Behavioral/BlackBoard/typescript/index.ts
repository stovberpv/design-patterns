import Blackboard from "./Blackboard/Blackboard";
import Control from "./Control/Control";

const bb = new Blackboard();

const control = new Control(bb);
control.loop();