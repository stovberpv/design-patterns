import KnowledgeSource from "./KnowledgeSource";

class KnowledgeDigitRecognition extends KnowledgeSource {
  protected updateBlackboard() : void {}
  execCondition() : void {}
  execAction() : void {}
}

export default KnowledgeDigitRecognition;