import KnowledgeSourceInterface from "./KnowledgeSourceInterface";
import BlackboardInterface from "../Blackboard/BlackBoardInterface";
import SolutionInterface from "../Blackboard/SolutionInterface";


abstract class KnowledgeSource implements KnowledgeSourceInterface {
  blackboard: BlackboardInterface;
  solution: SolutionInterface;

  constructor(blackboard : BlackboardInterface) {
    this.blackboard = blackboard;
  }

  protected abstract updateBlackboard() : void;
  abstract execCondition() : boolean;
  abstract execAction() : void;
}

export default KnowledgeSource;