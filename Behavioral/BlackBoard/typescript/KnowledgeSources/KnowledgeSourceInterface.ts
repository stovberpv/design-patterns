/**
 * Knowledge sources are separate, independent subsystems that solve specific aspects of the overall problem.
 * Together they model the overall problem domain.
 * None of them can solve the task of the system alone.
 * Knowledge sources do not communicate directly, they only read from and write to the blackboard.
 * An algorithm usually works on the results of other algorithms.
 * They therefore have to understand the vocabulary of the blackboard.
 * Each knowledge source is responsible for knowing the conditions under which it can contribute to a solution.
 *
 * Often a knowledge source operates on two levels of abstraction:
 *  - If a knowledge source implements forward reasoning, a particular solution is transformed to a higher-level solution.
 *  - A knowledge source that reasons backwards searches at a lower level for support for a solution, and may refer it back to a lower level if the reasoning did not give support for the solution.
 *
 * Knowledge sources are therefore split into a condition-part and an action-part:
 *  - The condition-part evaluates the current state of the solution process, as written on the blackboard, to determine if it can make a contribution.
 *  - The action-part produces a result that may cause a change to the blackboard's contents.
 *
 * The knowledge sources can be a special knowledge sources, witch do not contribute directly to solutions on the blackboard, but perform calculations on which control decisions are made.
 * Typical tasks are the estimation of the potential for progress, or the computational costs for execution of  knowledge sources.
 * Their results are called control data and are put on the blackboard as well.
 */
interface IKnowledgeSource {
  /**
   * Evaluates the current state of the solution process, as written on the blackboard, to determine if it can make a contribution.
   */
  execCondition() : boolean;
  /**
   * Produces a result that may cause a change to the blackboard's contents
   */
  execAction() : void;
}

export default IKnowledgeSource;