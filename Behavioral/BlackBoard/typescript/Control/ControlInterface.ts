import BlackboardInterface from "../Blackboard/BlackBoardInterface";

interface ControlInterface {
  constructor(blackBoard : BlackboardInterface);
  nextSource() : void;
  loop() : void;
}

export default ControlInterface;