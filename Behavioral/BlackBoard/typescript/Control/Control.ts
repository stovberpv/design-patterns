import BlackboardInterface from "../Blackboard/BlackBoardInterface";

/**
 * A central control component.
 * Evaluates the current state of  processing and coordinates the specialized programs.
 *
 * The Control component chooses a knowledge source to invoke, and a hypothesis or a set of hypotheses to be worked on.
 *
 * It applies the action-part of the knowledge source to the hypotheses.
 * The control component runs a loop that monitors the changes on the blackboard and decides what action to take next.
 * It schedules knowledge source evaluations and activations according to a knowledge application strategy.
 * The basis for this strategy is the data on the blackboard.
 */
class Control {
  blackBoard : BlackboardInterface;

  constructor(blackBoard : BlackboardInterface) {
    this.blackBoard = blackBoard;
  }

  /**
   * Procedure to select the next knowledge source.
   * Determines which knowledge sources are potential contributors by observing the blackboard.
   * Invokes the condition-part of each candidate knowledge source
   */
  nextSource() : void {
  }

  /**
   * Monitors the changes on the blackboard and decides what action to take next
   * Inspect the blackboard to determine if and how they can contribute to the current state of the solution.
   */
  loop() : void {

  }
}

export default Control;