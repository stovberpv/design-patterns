# Backboard

## Название

Классная доска

## Альтернативное название

TODO :

## Назначение

The  idea behind the Blackboard architecture is a collection of independent programs that work cooperatively on  a  common data structure.
Each program is specialized for solving a particular part of the overall task,  and all programs work together on the solution.
These specialized programs are independent of each other.
They do not call each other, nor is there a predetermined sequence for their activation.
Instead, the direction taken by the system is mainly determined by the current state of progress.
A central control component evaluates the current state of  processing and coordinates the specialized programs.
This data-directed control regime is referred to as opportunistic problem solving.
It makes experimentation with different algorithms possible, and allows experimentally-derived heuristics to control processing.

Provides a computational framework for the design and implementation of systems that integrate large and diverse specialized modules, and implement complex, non-deterministic control strategies. The Blackboard pattern shows how a complex problem, such as image or speech recognition can be broken up into smaller, specialized subsystems that work together to solve a problem.

[Doc](https://ff.tu-sofia.bg/~bogi/knigi/SE/Wiley%20-%20Pattern-Oriented%20Software%20Architecture%20-%20Volume%201,%20A%20System%20of%20Patterns.pdf)

### Своими словами

TODO :

## Когда применять

The Blackboard architectural pattern is useful for problems for which no determlnlstic solution strategies are known.
In Blackboard several specialized subsystems assemble their knowledge to bulld a possibly partial or approximate solution.

- speech recognition
- vehicle identification and tracking
- protein structure identification
- sonar signals interpretation

## Пример

TODO :

## Достоинства и недостатоки

TODO :

## Реализация

1. [php](./php/index.php)
1. [typescript](./typescript/index.ts)
