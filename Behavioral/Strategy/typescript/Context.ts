import IStrategy from './IStrategy';

class Context {
  private strategy : IStrategy;

  constructor(strategy : IStrategy) {
    this.strategy = strategy;
  }

  public doAction() : void {
    this.strategy.doAction();
  }
}

export default Context;