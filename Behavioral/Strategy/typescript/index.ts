import Context from './Context';
import StrategyA from './StrategyA';
import StrategyB from './StrategyB';

const ctxa = new Context(new StrategyA());
ctxa.doAction();

const ctxb = new Context(new StrategyB());
ctxb.doAction();