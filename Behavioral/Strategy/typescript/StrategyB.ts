import IStrategy from './IStrategy';

class StrategyB implements IStrategy {
  public doAction() : void {
    console.log('Strategy B action');
  }
}

export default StrategyB;