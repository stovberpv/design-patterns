import IStrategy from './IStrategy';

class StrategyA implements IStrategy {
  public doAction() : void {
    console.log('Strategy A action');
  }
}

export default StrategyA;