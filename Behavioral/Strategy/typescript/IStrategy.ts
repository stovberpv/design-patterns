interface IStrategy {
  doAction() : void;
}

export default IStrategy;