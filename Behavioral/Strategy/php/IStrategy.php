<?php

interface IStrategy
{
  public function doAction() : void;
}