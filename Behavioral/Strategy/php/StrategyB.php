<?php

require_once 'IStrategy.php';

class StrategyB implements IStrategy
{
  public function doAction() : void
  {
    echo "Strategy B\n";
  }
}