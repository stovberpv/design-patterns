<?php

require_once 'Context.php';
require_once 'StrategyA.php';
require_once 'StrategyB.php';

$ctxa = new Context(new StrategyA());
$ctxa->doAction();

$ctxb = new Context(new StrategyB());
$ctxb->doAction();