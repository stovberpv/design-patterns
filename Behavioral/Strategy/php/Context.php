<?php

require_once 'IStrategy.php';

class Context
{
  private $strategy;

  public function __construct(IStrategy $strategy)
  {
    $this->strategy = $strategy;
  }

  public function doAction()
  {
    $this->strategy->doAction();
  }
}