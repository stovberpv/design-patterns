<?php

require_once 'IStrategy.php';

class StrategyA implements IStrategy
{
  public function doAction() : void
  {
    echo "Strategy A\n";
  }
}