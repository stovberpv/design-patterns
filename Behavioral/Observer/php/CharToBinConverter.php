<?php

require_once 'Observer.php';
require_once 'Subject.php';

class CharToBinConverter extends Observer
{
  /**
   * @var Subject
   */
  private $char; # before PHP7.4
  // private Subject $char; # since PHP7.4

  public function __construct(Subject $char)
  {
    $this->char = $char;
    $this->char->attach($this);
  }

  public function update(Subject $char) : void
  {
    $char_array = str_split($char->getChar());

    $char_binaries = [];
    foreach ($char_array as $ch) {
      $char_binary     = unpack('H*', $ch);
      $char_binaries[] = base_convert($char_binary[1], 16, 2);
    }

    $converted = implode(' ', $char_binaries);
    echo "$converted\n";
  }
}