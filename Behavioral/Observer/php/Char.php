<?php

require_once 'Subject.php';

class Char extends Subject
{
  /**
   * @var String
   */
  private $char;

  public function __construct()
  {
    parent::__construct();
  }

  public function getChar() : string
  {
    return $this->char;
  }

  public function setChar(string $char) : void
  {
    $this->char = $char;

    $this->notify();
  }
}