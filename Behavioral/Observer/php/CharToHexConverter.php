<?php

require_once 'Observer.php';
require_once 'Subject.php';

class CharToHexConverter extends Observer
{
  /**
   * @var Subject
   */
  private $char; # before PHP7.4
  // private Subject $char; # since PHP7.4

  public function __construct(Subject $char)
  {
    $this->char = $char;
    $this->char->attach($this);
  }

  public function update(Subject $char) : void
  {
    $converted = bin2hex($char->getChar());
    echo "$converted\n";
  }
}