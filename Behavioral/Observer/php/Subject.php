<?php

require_once 'Observer.php';

abstract class Subject
{
  /**
   * @var Observer
   */
  protected $observers; # before PHP7.4
  // protected Observer $observers; # since PHP7.4

  public function __construct()
  {
    $this->observers = [];
  }

  public function attach(Observer $observer) : void
  {
    $this->observers[] = $observer;
  }

  public function detach(Observer $observer) : void
  {
    unset($this->observers[$observer]);
  }

  public function notify() : void
  {
    foreach ($this->observers as $observer) {
      $observer->update($this);
    }
  }
}