<?php

require_once 'Char.php';
require_once 'CharToHexConverter.php';
require_once 'CharToBinConverter.php';

$char = new Char();
$converterA = new CharToHexConverter($char);
$converterB = new CharToBinConverter($char);

$ch = 'ABC';
echo "Convert $ch to Hex & Bin\n";
$char->setChar($ch);

$ch = 10;
echo "Convert $ch to Hex & Bin\n";
$char->setChar($ch);