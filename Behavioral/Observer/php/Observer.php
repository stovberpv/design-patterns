<?php

require_once 'Subject.php';

abstract class Observer
{
  abstract function update(Subject $subject) : void;
}