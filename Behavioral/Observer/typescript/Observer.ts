import Subject from './Subject';

abstract class Observer {
  abstract update(s : Subject) : void;
}

export default Observer;