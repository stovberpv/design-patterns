import Observer from './Observer';
import ClockTimer from './ClockTimer';
import Subject from './Subject';

class DigitalClock extends Observer {
  private timer : ClockTimer;

  constructor(timer : ClockTimer) {
    super();
    this.timer = timer;
    this.timer.attach(this);
  }

  public update(s : Subject) : void {
    console.log('DigitalClock@update::tick');
  }
}

export default DigitalClock;