import ClockTimer from './ClockTimer';
import DigitalClock from './DigitalClock';

const timer : ClockTimer = new ClockTimer();
new DigitalClock(timer);

timer.start();
setTimeout(() => timer.stop(), 5000);