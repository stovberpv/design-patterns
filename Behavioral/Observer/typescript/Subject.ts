import Observer from './Observer';

abstract class Subject {
  protected observers : Array<Observer>;

  constructor() {
    this.observers = [];
  }

  public attach(observer : Observer) {
    this.observers.push(observer);
  }
  public detach(observer : Observer) {
    this.observers = this.observers.filter(o => o !== observer);
  }
  public notify() {
    this.observers.forEach(o => o.update(this));
  }
}

export default Subject;