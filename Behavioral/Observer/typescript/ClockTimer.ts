import Subject from './Subject';

class ClockTimer extends Subject {
  private timerId : number = null;

  constructor() {
    super();
  }

  public start() {
    this.timerId = setInterval(() => this.tick(), 1000);
  }
  public stop() {
    clearInterval(this.timerId);
    this.timerId = null;
  }
  public tick() {
    super.notify();
  }
}

export default ClockTimer;