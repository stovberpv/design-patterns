# Design patterns

1. [Behavioral](/Behavioral/README.md)
    1. [Blackboard](/Behavioral/BlackBoard/README.md)
    1. Chain of responsibility
    1. Command
    1. Interpreter
    1. Iterator
    1. Mediator
    1. Memento
    1. Null object
    1. [Observer](/Behavioral/Observer/README.md)
    1. Servant
    1. Specification
    1. State
    1. [Strategy](/Behavioral/Strategy/README.md)
    1. Template method
    1. Visitor

1. [Creational](/Creational/README.md)
    1. [AbstractFactory](/Creational/AbstractFactory/README.md)
    1. Builder
    1. Dependency Injection
    1. [FactoryMethod](/Creational/FactoryMethod/README.md)
    1. Lazy initialization
    1. Multiton
    1. Object pool
    1. Prototype
    1. Resource acquisition is initialization (RAII)
    1. [Singleton](/Creational/Singleton/README.md)

1. [Structural](/Structural/README.md)
    1. Adapter
    1. Bridge
    1. Composite
    1. [Decorator](/Structural/Decorator/README.md)
    1. Extension object
    1. Facade
    1. Flyweight
    1. Front controller
    1. Marker
    1. Module
    1. Proxy
    1. Twin

1. Concurrency
    1. Active Object
    1. Balking
    1. Binding properties
    1. Compute kernel
    1. Double-checked locking
    1. Event-based asynchronous
    1. Guarded suspension
    1. Join
    1. Lock
    1. Messaging design pattern (MDP)
    1. Monitor object
    1. Reactor
    1. Read-write lock
    1. Scheduler
    1. Thread pool
    1. Thread-specific storage
